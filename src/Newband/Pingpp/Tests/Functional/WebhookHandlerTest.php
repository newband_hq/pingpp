<?php

namespace Newband\Pingpp\Tests;

use JMS\Serializer\SerializerBuilder;
use Newband\Pingpp\Webhook\WebhookHandler;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WebhookHandlerTest
 * @package Newband\Pingpp\Tests
 * @author Zafar <zafar@newband.com>
 */
class WebhookHandlerTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        \Doctrine\Common\Annotations\AnnotationRegistry::registerAutoloadNamespace(
            'JMS\Serializer\Annotation', __DIR__.'/../../../../../vendor/jms/serializer/src'
        );
    }

    public function testDeserialization()
    {
        $serializer = SerializerBuilder::create()->build();
        $jsonFile = __DIR__.'/../Resources/charge.json';
        $json = file_get_contents($jsonFile);
        $request = new Request(array(), array(), array(), array(), array(), array(), $json);
        $webhookHandler = new WebhookHandler($serializer);
        $this->assertInstanceOf('Newband\Pingpp\Event', $webhookHandler->handleRequest($request));
    }

    public function testDeserializationWithWrongData()
    {
        $serializer = SerializerBuilder::create()->build();
        $request = new Request(array(), array(), array(), array(), array(), array(), null);
        $webhookHandler = new WebhookHandler($serializer);
        $this->assertNull($webhookHandler->handleRequest($request));
    }
}