<?php

namespace Newband\Pingpp\Tests;

use JMS\Serializer\SerializerBuilder;

/**
 * Class EventDeserializationTest
 * @package Newband\Pingpp\Tests
 * @author Zafar <zafar@newband.com>
 */
class EventDeserializationTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        \Doctrine\Common\Annotations\AnnotationRegistry::registerAutoloadNamespace(
            'JMS\Serializer\Annotation', __DIR__.'/../../../../../vendor/jms/serializer/src'
        );
    }

    public function testDeserialization()
    {
        $serializer = SerializerBuilder::create()->build();
        $jsonFile = __DIR__.'/../Resources/charge.json';
        $json = file_get_contents($jsonFile);
        $event = $serializer->deserialize($json, 'Newband\Pingpp\Event', 'json');
        $this->assertInstanceOf('Newband\Pingpp\Event', $event);
    }
}