<?php

namespace Newband\Pingpp;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class EventObject
 *
 * @Serializer\Discriminator(field="object", map={"charge":"Newband\Pingpp\Charge"})
 *
 * @package Newband\Pingpp
 * @author Zafar <zafar@newband.com>
 */
abstract class EventObject
{
    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $id;

    /**
     * @Serializer\Type("string")
     *
     * @var int
     */
    protected $created;

    /**
     * @Serializer\Type("boolean")
     *
     * @var bool
     */
    protected $liveMode;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return boolean
     */
    public function isLiveMode()
    {
        return $this->liveMode;
    }

    /**
     * @param boolean $liveMode
     */
    public function setLiveMode($liveMode)
    {
        $this->liveMode = $liveMode;
    }
}