<?php

namespace Newband\Pingpp;

/**
 * Class Pingpp
 * @package Newband\Pingpp
 * @author Zafar <zafar@newband.com>
 */
class Pingpp
{
    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $app;

    /**
     * @var string
     */
    private $error;

    /**
     * Pingpp constructor.
     * @param string $apiKey
     * @param string $app
     */
    public function __construct($apiKey, $app){
        $this->apiKey = $apiKey;
        $this->app = $app;
    }

    /**
     * @param Charge $charge
     * @return null|\Pingpp\Charge
     */
    public function createCharge(Charge $charge)
    {
        \Pingpp\Pingpp::setApiKey($this->apiKey);
        try {
            $data = array(
                'order_no'  => $charge->getOrderNumber(),
                'amount'    => $charge->getAmount(),
                'app'       => array('id' => $this->app),
                'channel'   => $charge->getChannel(),
                'currency'  => $charge->getCurrency(),
                'client_ip' => $charge->getClientIp(),
                'subject'   => $charge->getSubject(),
                'body'      => $charge->getBody()
            );
            $createChargeRequest = \Pingpp\Charge::create($data);
            return $createChargeRequest;
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
        }

        return null;
    }

    /**
     * @param string $chargeId
     * @return null|\Pingpp\Charge
     */
    public function retrieveCharge($chargeId)
    {
        \Pingpp\Pingpp::setApiKey($this->apiKey);

        try {
            return \Pingpp\Charge::retrieve($chargeId);
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return bool
     */
    public function hasError()
    {
        return !is_null($this->error);
    }
}