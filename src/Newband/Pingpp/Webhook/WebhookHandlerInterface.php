<?php

namespace Newband\Pingpp\Webhook;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface WebhookHandlerInterface
 * @package Newband\Pingpp\Webhook
 * @author Zafar <zafar@newband.com>
 */
interface WebhookHandlerInterface
{
    /**
     * @param Request $request
     * @return \Newband\Pingpp\Event
     */
    public function handleRequest(Request $request);
}