<?php

namespace Newband\Pingpp\Webhook;

use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WebhookHandler
 * @package Newband\Pingpp\Webhook
 * @author Zafar <zafar@newband.com>
 */
class WebhookHandler implements WebhookHandlerInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var string
     */
    private $error;

    /**
     * WebhookHandler constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(
        SerializerInterface $serializer
    ){
        $this->serializer = $serializer;
    }

    /**
     * {@inheritDoc}
     */
    public function handleRequest(Request $request)
    {
        try {
            return $this->serializer->deserialize($request->getContent(), 'Newband\Pingpp\Event', 'json');
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return bool
     */
    public function hasError()
    {
        return !is_null($this->error);
    }
}