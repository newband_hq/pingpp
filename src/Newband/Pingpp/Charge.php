<?php

namespace Newband\Pingpp;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Charge
 *
 * @package Newband\Pingpp
 * @author Zafar <zafar@newband.com>
 *
 */
class Charge extends EventObject
{
    /**
     * @Serializer\Type("boolean")
     *
     * @var bool
     */
    protected $paid;

    /**
     * @Serializer\Type("boolean")
     *
     * @var bool
     */
    protected $refund;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $app;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $channel;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $orderNumber;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $clientIp;

    /**
     * @Serializer\Type("double")
     *
     * @var number
     */
    protected $amount;

    /**
     * @Serializer\Type("double")
     *
     * @var number
     */
    protected $amountSettle;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $currency;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $subject;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $body;

    /**
     * @Serializer\Type("integer")
     *
     * @var int
     */
    protected $timePaid;

    /**
     * @Serializer\Type("integer")
     *
     * @var int
     */
    protected $timeExpire;

    /**
     * @Serializer\Type("integer")
     *
     * @var int
     */
    protected $timeSettle;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $transactionNumber;

    /**
     * @Serializer\Type("double")
     *
     * @var number
     */
    protected $amountRefunded;

    /**
     * @Serializer\Type("integer")
     *
     * @var int
     */
    protected $failureCode;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $failureMessage;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $description;

    /**
     * @Serializer\Type("array<string, string>")
     *     
     * @var array
     */
    protected $metadata = array();

    /**
     * @return boolean
     */
    public function isPaid()
    {
        return $this->paid;
    }

    /**
     * @param boolean $paid
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
    }

    /**
     * @return boolean
     */
    public function isRefund()
    {
        return $this->refund;
    }

    /**
     * @param boolean $refund
     */
    public function setRefund($refund)
    {
        $this->refund = $refund;
    }

    /**
     * @return string
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * @param string $app
     */
    public function setApp($app)
    {
        $this->app = $app;
    }

    /**
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param string $channel
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return string
     */
    public function getClientIp()
    {
        return $this->clientIp;
    }

    /**
     * @param string $clientIp
     */
    public function setClientIp($clientIp)
    {
        $this->clientIp = $clientIp;
    }

    /**
     * @return number
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param number $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return number
     */
    public function getAmountSettle()
    {
        return $this->amountSettle;
    }

    /**
     * @param number $amountSettle
     */
    public function setAmountSettle($amountSettle)
    {
        $this->amountSettle = $amountSettle;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return int
     */
    public function getTimePaid()
    {
        return $this->timePaid;
    }

    /**
     * @param int $timePaid
     */
    public function setTimePaid($timePaid)
    {
        $this->timePaid = $timePaid;
    }

    /**
     * @return int
     */
    public function getTimeExpire()
    {
        return $this->timeExpire;
    }

    /**
     * @param int $timeExpire
     */
    public function setTimeExpire($timeExpire)
    {
        $this->timeExpire = $timeExpire;
    }

    /**
     * @return int
     */
    public function getTimeSettle()
    {
        return $this->timeSettle;
    }

    /**
     * @param int $timeSettle
     */
    public function setTimeSettle($timeSettle)
    {
        $this->timeSettle = $timeSettle;
    }

    /**
     * @return string
     */
    public function getTransactionNumber()
    {
        return $this->transactionNumber;
    }

    /**
     * @param string $transactionNumber
     */
    public function setTransactionNumber($transactionNumber)
    {
        $this->transactionNumber = $transactionNumber;
    }

    /**
     * @return number
     */
    public function getAmountRefunded()
    {
        return $this->amountRefunded;
    }

    /**
     * @param number $amountRefunded
     */
    public function setAmountRefunded($amountRefunded)
    {
        $this->amountRefunded = $amountRefunded;
    }

    /**
     * @return int
     */
    public function getFailureCode()
    {
        return $this->failureCode;
    }

    /**
     * @param int $failureCode
     */
    public function setFailureCode($failureCode)
    {
        $this->failureCode = $failureCode;
    }

    /**
     * @return string
     */
    public function getFailureMessage()
    {
        return $this->failureMessage;
    }

    /**
     * @param string $failureMessage
     */
    public function setFailureMessage($failureMessage)
    {
        $this->failureMessage = $failureMessage;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }
}