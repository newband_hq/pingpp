<?php

namespace Newband\Pingpp;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class EventData
 * @package Newband\Pingpp
 * @author Zafar <zafar@newband.com>
 */
class EventData
{
    /**
     * @Serializer\Type("Newband\Pingpp\EventObject")
     *
     * @var EventObject
     */
    protected $object;

    /**
     * @return EventObject
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param EventObject $object
     */
    public function setObject(EventObject $object)
    {
        $this->object = $object;
    }
}