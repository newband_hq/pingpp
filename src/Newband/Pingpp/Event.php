<?php

namespace Newband\Pingpp;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Event
 * @package Newband\Pingpp
 * @author Zafar <zafar@newband.com>
 */
class Event
{
    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $id;

    /**
     * @Serializer\Type("integer")
     *
     * @var int
     */
    protected $created;

    /**
     * @Serializer\Type("boolean")
     * @Serializer\SerializedName("livemode")
     *
     * @var bool
     */
    protected $liveMode;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $type;

    /**
     * @Serializer\Type("Newband\Pingpp\EventData")
     *
     * @var EventData
     */
    protected $data;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $object;

    /**
     * @Serializer\Type("integer")
     * @Serializer\SerializedName("pending_webhooks")
     *
     * @var int
     */
    protected $pendingWebHooks;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    protected $request;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return boolean
     */
    public function isLiveMode()
    {
        return $this->liveMode;
    }

    /**
     * @param boolean $liveMode
     */
    public function setLiveMode($liveMode)
    {
        $this->liveMode = $liveMode;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return EventData
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param EventData $data
     */
    public function setData(EventData $data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param string $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return int
     */
    public function getPendingWebHooks()
    {
        return $this->pendingWebHooks;
    }

    /**
     * @param int $pendingWebHooks
     */
    public function setPendingWebHooks($pendingWebHooks)
    {
        $this->pendingWebHooks = $pendingWebHooks;
    }

    /**
     * @return string
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param string $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }
}